*** Settings ***
Library  Selenium2Library
Library  Screenshot

Suite Setup     Open Browser    ${URL}   ${BROWSER}
Suite Teardown  Close All Browsers

*** Variables ***
${URL}              http://www.google.com
${BROWSER}          Chrome
${search_form}      css=form[name=f]
${search_query}     css=input[name=q]
${search_term_1}      Bahamas
${search_term_2}      Amsterdam

*** Test Cases ***
Google_Bahamas_Search
	Maximize Browser Window
    Wait Until Element Is Visible  ${search_form}
    Wait Until Element Is Visible  ${search_query}
    Input Text      ${search_query}   ${EMPTY}
    Input Text      ${search_query}   ${search_term_1}
    Submit Form
	Take Screenshot
Google_Amsterdam_Search
	Maximize Browser Window
    Wait Until Element Is Visible  ${search_form}
    Wait Until Element Is Visible  ${search_query}
    Input Text      ${search_query}   ${EMPTY}
    Input Text      ${search_query}   ${search_term_2}
    Submit Form
	Take Screenshot